#include "Vista.h"

Vista::Vista(){}

void Vista::MenuPrincipal()
{	
	int op;
	do {
		system("cls");

		cout << "************************************" << endl;
		cout << "******* T I M B I R I C H E ********" << endl;
		cout << "************************************" << "\n\n\n\n";

		cout << "Bienvenido al juego Timbiriche!" << "\n\n\n";

		cout << "1. Humano vs Maquina" << endl;
		cout << "2. Humano vs Humano" << endl;
		cout << "3. Salir" << "\n\n";

		cout << "Digite una opcion (EX: 1-3)" << endl;
		op = PedirOpcion();

		switch (op) {
		case 1:
			this->Instrucciones();
			this->MenuJuegoVsMaquina();
			break;
		case 2:
			this->Instrucciones();
			this->MenuJuegoVsHumano();
			break;
		case 3:
			cout << "Saliendo....";
			break;
		default:
			cout << "Debe digitar un numero entre 1-3 solamente\n\n";
			system("pause");
			break;
		}
	} while (op != 3);

}

void Vista::MenuJuegoVsMaquina()
{
	system("cls");
	string j1 = ObtenerNombreJugador();
	string j2 = "MAQUINA";
	if (j1[0] == 'M') {
		j2 = "COMPUTADORA";
	}

	int tam = ObtenerTamanioMatriz();
	Juego* juego = new Juego(tam, j1, j2);
	string jugada = "";
	while (!juego->JuegoTerminado()) {
		system("cls");
		string jugador = juego->sigJugador;

		if (jugador == j2) {
			JugadaMaquina(j2, juego);
			system("pause");
		}
		else {
			juego->MostrarResumen();
			jugada = ObtenerJugada(jugador);
			juego->JugarTurno(jugador, jugada);
			system("pause");
		}
	}

	if (jugada != "q") {
		juego->MostrarResultados();
	}
}

void Vista::MenuJuegoVsHumano()
{
	system("cls");
	cout << "Jugador 1" << endl;
	string j1 = ObtenerNombreJugador();
	cout << "Jugador 2" << endl;
	string j2 = ObtenerNombreJugador();
	if (j1[0] == j2[0]) {
		cout << "\nJugador1 y Jugador2 comparten la misma inicial: " << j1[0] << endl;
		cout << "Nombres seran reemplazados por 'Uno' y 'Dos'." << endl;
		j1 = "Uno";
		j2 = "Dos";
		system("pause");
	}

	int tam = ObtenerTamanioMatriz();
	Juego* juego = new Juego(tam, j1, j2);
	string jugada = "";
	while (!juego->JuegoTerminado()) {
		system("cls");
		string jugador = juego->sigJugador;
		juego->MostrarResumen();
		jugada = ObtenerJugada(jugador);
		juego->JugarTurno(jugador, jugada);
		system("pause");
	}

	if (jugada != "q") {
		juego->MostrarResultados();
	}
}

void Vista::Instrucciones()
{
	system("cls");
	cout << "El juego se desarrolla en una cuadricula cuyo tamanio esta determinado \npor la cantidad de cuadrados que se pueden formar uniendo puntos de la cuadricula." << endl;
	cout << "\nEjemplo cuadricula de tamanio 2x2:\n\n";
	cout << "a + + +" << endl;
	cout << "       " << endl;
	cout << "b + + +" << endl;
	cout << "       " << endl;
	cout << "c + + +" << endl;
	cout << "       " << endl;
	cout << "  1 2 3" << "\n\n";

	cout << "Se deben unir puntos para formar cuadrados. El jugador con la mayor cantidad de cuadrados gana." << endl;
	cout << "Para unir dos puntos se deben digitar sus coordenadas." << "\n\n";
	cout << "Ejemplo: a1b1 \n" << endl;

	cout << "a + + +" << endl;
	cout << "  |    " << endl;
	cout << "b + + +" << endl;
	cout << "       " << endl;
	cout << "c + + +" << endl;
	cout << "       " << endl;
	cout << "  1 2 3" << endl << "\n\n";

	system("pause");

}

void Vista::JugadaMaquina(string jugador, Juego* juego)
{
	Punto* p = NULL;
	while (p == NULL)
	{
		for (int i = 0; i < juego->tablero->m && p == NULL; i++)
		{
			for (int j = 1; j < juego->tablero->n && p == NULL; j++)
			{
				if (i % 2 == 0 && j % 2 == 1 || i % 2 == 1 && j % 2 == 0) {
					if (juego->tablero->matriz[i][j] == ' ' && ObtenerAleatorio() % 2 > 0) {
						p = new Punto(i, j);
						juego->JugarTurnoMaquina(jugador, p);
					}
				}
			}
		}
	}
}

string Vista::ConvertirMinusculas(string str)
{
	for_each(str.begin(), str.end(), [](char& c) {
		c = ::tolower(c);
		});
	return str;
}

string Vista::ConvertirMayusculas(string str)
{
	for_each(str.begin(), str.end(), [](char& c) {
		c = ::toupper(c);
		});
	return str;
}

string Vista::ObtenerJugada(string jugador)
{
	string entrada;
	cout << jugador <<" digita los puntos a conectar ('q' para salir): ";   cin >> entrada;
	cout << endl;
	cin.clear();
	cin.ignore(80, '\n');
	return ConvertirMinusculas(entrada);
}

int Vista::PedirOpcion()
{
	int op;
	cin >> op;
	cout << endl;
	cin.clear();
	cin.ignore(80, '\n');
	return op;
}

string Vista::ObtenerNombreJugador()
{
	string nombre = "";
	while (nombre == "") {
		cout << "Digite su nombre" << endl;
		cin >> nombre;
		cout << endl;
		cin.clear();
		cin.ignore(80, '\n');
	}
	system("cls");
	return ConvertirMayusculas(nombre);
}

int Vista::ObtenerTamanioMatriz()
{
	int tamanio = 0;
	while (tamanio < 2 || tamanio > 9) {
		system("cls");
		cout << "De que tamanio desea la matriz ? (2...9)\n";
		cin >> tamanio;
		cout << endl;
		cin.clear();
		cin.ignore(80, '\n');
	}
	return tamanio;
}

int Vista::ObtenerAleatorio()
{
	int aleatorio;
	srand(time(NULL));
	aleatorio = rand() % 10 + 1;
	return aleatorio;
}
