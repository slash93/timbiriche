#pragma once
#include "Punto.h"

class Linea
{
public:
	Punto* pA;
	Punto* pB;
	Punto* enlace;
	Linea(Punto* pA, Punto* pB);
	Linea(Punto* pA, Punto* pB, Punto* enlace);
};

