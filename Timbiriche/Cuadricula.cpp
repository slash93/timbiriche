#include "Cuadricula.h"

Cuadricula::Cuadricula(int tam)
{
	this->tam = tam;
	if (tam >= 2 && tam <= 9) {
		m = (2 * (tam+1) - 1);
		n = (2 * (tam+1) - 1);
		matriz = new char* [m];
		for (int i = 0; i < m; i++) {
			matriz[i] = new char[n];
			for (int j = 0; j < n; j++) {
				if ( (i%2 + j%2) == 0) {
					matriz[i][j] = '+';
				}
				else {
					matriz[i][j] = ' ';
				}
			}
		}
	}
	else {
		cout << "Los tamanios permitidos son: 2, 3, 4, 5, 6, 7, 8, 9" << endl;
	}
}

Cuadricula::~Cuadricula()
{
}

void Cuadricula::Mostrar()
{
	int fila = 97;
	string salida = "";
	int counter = 1;
	string cols = "  ";
	for (int i = 0; i < m; i++) {
		if (i % 2 == 0) {
			salida += (fila + i/2);
			salida += " ";
			cols += to_string(counter);
			counter++;
		}
		else {
			salida += "  ";
			cols += " ";
		}

		for (int j = 0; j < n; j++) {
			salida += matriz[i][j];
		}
		salida += "\n";
	}


	cout << salida << endl << cols << "\n\n";
}

void Cuadricula::CrearLinea(Linea* l)
{
	char letra = '-';
	if (l->pA->c == l->pB->c) {
		letra = '|';
	}
	matriz[l->enlace->f][l->enlace->c] = letra;
}

Linea* Cuadricula::ValidarLinea(Linea* l)
{
	Punto* enlace = NULL;

	Punto* arr = new Punto(l->pA->f - 2, l->pA->c);
	Punto* aba = new Punto(l->pA->f + 2, l->pA->c);
	Punto* izq = new Punto(l->pA->f, l->pA->c - 2);
	Punto* der = new Punto(l->pA->f, l->pA->c + 2);

	Punto* temp = NULL;

	if (ValidarPunto(arr) && enlace == NULL) {
		temp = new Punto(arr->f + 1, arr->c);
		if (arr->EsIgual(l->pB) && PuntoLibre(temp)) {
			enlace = temp;
		}
	}

	if (ValidarPunto(aba) && enlace == NULL) {
		temp = new Punto(aba->f - 1, aba->c);
		if (aba->EsIgual(l->pB) && PuntoLibre(temp)) {
			enlace = temp;
		}
	}

	if (ValidarPunto(izq) && enlace == NULL) {
		temp = new Punto(izq->f, izq->c + 1);
		if (izq->EsIgual(l->pB) && PuntoLibre(temp)) {
			enlace = temp;
		}
	}

	if (ValidarPunto(der) && enlace == NULL) {
		temp = new Punto(der->f, der->c - 1);
		if (der->EsIgual(l->pB) && PuntoLibre(temp)) {
			enlace = temp;
		}
	}

	l->enlace = enlace;

	return l;
}

bool Cuadricula::ValidarRangos(string linea)
{
	bool valida = true;
	if (linea.length() != 4) {
		valida = false;
	}
	else {
		string pt1 = linea.substr(0, 2);
		string pt2 = linea.substr(2, 4);
		valida = ValidarPunto(pt1) && ValidarPunto(pt2);
	}
	return valida;
}

bool Cuadricula::PuntoLibre(Punto* p)
{
	return matriz[p->f][p->c] == ' ';
}

bool Cuadricula::ValidarCuadrado(Punto* p)
{
	bool completo = true;
	Punto** pts = new Punto*[4];
	pts[0] = new Punto(p->f - 1, p->c); // arr 
	pts[1] = new Punto(p->f + 1, p->c);	// aba 
	pts[2] = new Punto(p->f, p->c - 1);	// izq 
	pts[3] = new Punto(p->f, p->c + 1);	// der 

	int i = 0;
	while (i < 4 && completo)
	{
		completo = !PuntoLibre(pts[i]);
		i++;
	}
	return completo;
}

int Cuadricula::TotalCuadrados()
{
	return this->tam*this->tam;
}

bool Cuadricula::ValidarPunto(string punto)
{
	bool valido = true;
	if (punto.length() != 2) {
		valido = false;
	}
	else {
		int fila = 2*(punto[0] - 97);
		int col  = 2*(punto[1]-49);
		if (fila > m || col > n) {
			valido = false;
		}
	}
	return valido;
}

bool Cuadricula::ValidarPunto(Punto* p)
{
	return (p->f >= 0 && p->f < m) && (p->c >= 0 && p->c < n);
}

bool Cuadricula::EjecutarJugada(string linea)
{
	bool valida = false;
	if (ValidarRangos(linea)) {
		Punto* pA = new Punto(linea.substr(0, 2));
		Punto* pB = new Punto(linea.substr(2, 4));
		Linea* l = new Linea(pA, pB);
		l= ValidarLinea(l);
		if (l->enlace != NULL) {
			valida = true;
			CrearLinea(l);
		}
	}
	return valida;
}
