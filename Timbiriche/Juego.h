#pragma once
#include <iostream>
#include <sstream>
#include "Cuadricula.h"

using namespace std;

class Juego
{
public:
	Cuadricula* tablero;
	string j1, j2, sigJugador;
	int ptsJ1, ptsJ2,turno;
	bool terminado;
	Juego(int tam, string j1, string j2);
	void JugarTurno(string jugador, string linea);
	void JugarTurnoMaquina(string jugador, Punto* enlace);
	void MostrarTablero();
	void MostrarResumen();
	bool JuegoTerminado();
	void RefrescarPuntaje(string jugador);
	void SumarPunto(string jugador);
	void MostrarResultados();
	int ObtenerPuntaje(string jugador);
	string ObtenerOtroJugador(string jugador);
};

