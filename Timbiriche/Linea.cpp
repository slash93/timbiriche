#include "Linea.h"

Linea::Linea(Punto* pA, Punto* pB)
{
	this->pA = pA;
	this->pB = pB;
	this->enlace = NULL;
}

Linea::Linea(Punto* pA, Punto* pB, Punto* enlace)
{
	this->pA = pA;
	this->pB = pB;
	this->enlace = enlace;
}
