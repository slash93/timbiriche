#include "Punto.h"

Punto::Punto(int f, int c)
{
	this->f = f;
	this->c = c;
}

Punto::Punto(string punto)
{
	this->f = 2*(punto[0] - 97);
	this->c = 2*(punto[1] - 49);
}

bool Punto::EsIgual(Punto* p)
{
	return this->f == p->f && this->c == p->c;
}
