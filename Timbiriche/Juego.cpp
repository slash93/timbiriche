#include "Juego.h"

Juego::Juego(int tam, string j1, string j2)
{
	this->j1 = j1;
	this->j2 = j2;
	this->turno = 1;
	this->terminado = false;
	ptsJ1 = ptsJ2 = 0;
	tablero = new Cuadricula(tam);
	sigJugador = j1;
}

void Juego::JugarTurno(string jugador, string linea)
{
	if (linea == "q") {
		terminado = true;
	}
	else {
		if (tablero->EjecutarJugada(linea)) {
			RefrescarPuntaje(jugador);
			cout << "\nJugada de " << jugador << " completada\n\n";
			MostrarTablero();
			this->turno++;
		}
		else {
			cout << "Jugada invalida" << endl;
		}
	}
}

void Juego::JugarTurnoMaquina(string jugador, Punto* enlace)
{
	char letra = '-';
	if (enlace->f % 2 == 1 && enlace->c%2 ==0) {
		letra = '|';
	}
	this->tablero->matriz[enlace->f][enlace->c] = letra;
	RefrescarPuntaje(jugador);
	cout << "\nJugada de " << jugador << " completada\n\n";
	MostrarTablero();
	this->turno++;
}

void Juego::MostrarTablero()
{
	tablero->Mostrar();
}

void Juego::MostrarResumen()
{
	int fila = 97;
	string salida = "";
	int counter = 1;
	string cols = "  ";
	for (int i = 0; i < tablero->m; i++) {
		if (i % 2 == 0) {
			salida += (fila + i / 2);
			salida += " ";
			cols += to_string(counter);
			counter++;
		}
		else {
			salida += "  ";
			cols += " ";
		}

		for (int j = 0; j < tablero->n; j++) {
			salida += tablero->matriz[i][j];
		}

		if (i == 0) {
			salida = salida + "     Turno=" + to_string(this->turno);
		}

		if (i == 2) {
			salida = salida + "     " + this->j1[0] + "=" + to_string(this->ptsJ1);
		}

		if (i == 4) {
			salida = salida + "     " + this->j2[0] + "=" + to_string(this->ptsJ2);
		}

		salida += "\n";
	}

	cout << salida << endl << cols << "\n\n";
}

bool Juego::JuegoTerminado()
{
	return terminado;
}

void Juego::RefrescarPuntaje(string jugador)
{
	int cuadrados = 0;
	int ptAnt = ObtenerPuntaje(jugador);
	for (int i = 1; i < tablero->m; i+=2)
	{
		for (int j = 1; j < tablero->n; j+=2)
		{
			if (tablero->matriz[i][j] == ' ') {
				Punto* p = new Punto(i, j);
				if (tablero->ValidarCuadrado(p)) {
					tablero->matriz[i][j] = jugador[0];
					SumarPunto(jugador);
					cuadrados++;
				}
			}
			else {
				cuadrados++;
			}
		}
	}

	int ptAct = ObtenerPuntaje(jugador);
	if (ptAnt < ptAct) {
		this->sigJugador = jugador;
	}
	else {
		this->sigJugador = ObtenerOtroJugador(jugador);
	}

	this->terminado = (cuadrados == tablero->TotalCuadrados());
}

void Juego::SumarPunto(string jugador)
{
	if (this->j1 == jugador) {
		this->ptsJ1++;
	}
	else {
		this->ptsJ2++;
	}
}

void Juego::MostrarResultados()
{
	system("cls");
	this->MostrarResumen();
	string salida = "\n\n\n";

	if (ptsJ1 == ptsJ2) {
		salida += "Terminaron empatados!!!\n\n";
	}
	else if(ptsJ1 > ptsJ2) {
		salida = salida + "El ganador es: " + j1 + " !!!\n\n";
	}
	else {
		salida = salida + "El ganador es: " + j2 + " !!!\n\n";
	}

	cout << salida;

	system("pause");
}

int Juego::ObtenerPuntaje(string jugador)
{
	int puntaje = this->ptsJ1;
	if (jugador != this->j1) {
		puntaje = this->ptsJ2;
	}
	return puntaje;
}

string Juego::ObtenerOtroJugador(string jugador)
{
	string otro = this->j2;
	if (jugador != this->j1) {
		otro = this->j1;
	}
	return otro;
}
