#pragma once
#include <iostream>
#include <algorithm>
#include <iostream>
#include "Juego.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
using namespace std;

class Vista
{
public:
	Vista();
	void MenuPrincipal();
	void MenuJuegoVsMaquina();
	void MenuJuegoVsHumano();
	void Instrucciones();
	void JugadaMaquina(string jugador, Juego* juego);
	string ConvertirMinusculas(string str);
	string ConvertirMayusculas(string str);
	string ObtenerJugada(string jugador);
	int PedirOpcion();
	string ObtenerNombreJugador();
	int ObtenerTamanioMatriz();
	int ObtenerAleatorio();
};

