#pragma once
#include <iostream>
#include "Linea.h"
#include <sstream>
using namespace std;

class Cuadricula{
	public:
		char** matriz;
		int m, n, tam;
		Cuadricula(int tam);
		virtual ~Cuadricula();
		void Mostrar();
		void CrearLinea(Linea* l);
		Linea* ValidarLinea(Linea* l);
		bool ValidarPunto(string punto);
		bool ValidarPunto(Punto* p);
		bool EjecutarJugada(string mov);
		bool ValidarRangos(string mov);
		bool PuntoLibre(Punto* p);
		bool ValidarCuadrado(Punto* p);
		int TotalCuadrados();

};

